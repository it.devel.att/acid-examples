# Postgres
[Transaction Isolation Docs](https://www.postgresql.org/docs/9.5/transaction-iso.html)

```sh
docker-compose up
```
Connect to db
```sh
docker exec -it db psql -U root -d test
```

Create table
```psql
CREATE TABLE transactions (
	id SERIAL,
	user_id int not null,
	amount int not null default 0
);
```
Insert some data
```psql
INSERT INTO transactions(user_id, amount) VALUES (1, 100);
INSERT INTO transactions(user_id, amount) VALUES (2, 200);
```
Next need to open 2 terminals

T1 - Terminal 1
T2 - Terminal 2

#### READ-COMMITED ISOLATION
```console
test=# SHOW default_transaction_isolation;
 default_transaction_isolation 
-------------------------------
 read committed
(1 row)
```
T1
```console
test=# BEGIN;
BEGIN
test=# SELECT count(*) FROM transactions;
 count 
-------
     2
(1 row)
```
T2 (insert new row by transaction in second terminal)
```psql
BEGIN;
INSERT INTO transactions(user_id, amount) VALUES(3, 300);
COMMIT;
```
T1 (now our first transaction see 3 rows but have 2 as count(*)
```console
test=# BEGIN;
BEGIN
test=# SELECT count(*) FROM transactions;
 count 
-------
     2
(1 row)

test=# SELECT * FROM transactions;
 id | user_id | amount 
----+---------+--------
  2 |       2 |    200
  1 |       1 |    100
  3 |       3 |    300
(3 rows)
```
#### REPEATABLE-READ ISOLATION
##### Read
T1
```console
test=# BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
BEGIN
test=# SELECT count(*) FROM transactions;
 count 
-------
     3
(1 row)
```
T2 (insert new row in second transaction)
```console
test=# BEGIN;
BEGIN
test=# INSERT INTO transactions(user_id, amount) VALUES(3, 300);
INSERT 0 1
test=# COMMIT;
COMMIT
test=# BEGIN;
BEGIN
test=# INSERT INTO transactions(user_id, amount) VALUES(3, 300);
INSERT 0 1
test=# COMMIT;
COMMIT

test=# SELECT * FROM transactions ORDER by id;
 id | user_id | amount 
----+---------+--------
  1 |       1 |    100
  2 |       2 |    200
  3 |       3 |    300
  4 |       3 |    300
(4 rows)
```
T1 (see only 3 rows despite of previous transaction commit new changes)
```console
test=# BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
BEGIN
test=# SELECT count(*) FROM transactions;
 count 
-------
     3
(1 row)
(in this moment we insert new row in second termminal)
test=# SELECT * FROM transactions;
 id | user_id | amount 
----+---------+--------
  2 |       2 |    200
  1 |       1 |    100
  3 |       3 |    300
(3 rows)
```
##### Update
T1 (start new transaction)
```
BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
```
T2 (start new transaction)
```
BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
```
T1 update row
```
UPDATE transactions SET amount = amount + 100 WHERE user_id = 1;
COMMIT;
```
T2 try to update same row
```
UPDATE transactions SET amount = amount + 100 WHERE user_id = 1;
ERROR:  could not serialize access due to concurrent update
```
We see `ERROR:  could not serialize access due to concurrent update` because
> a repeatable read transaction cannot modify or lock rows changed by other transactions after the repeatable read transaction began.

But if first transaction will rollback, second transaction will commit successfully

##### Lock for update (isolation level read commited)

Create a little bit another transactions table
```psql
CREATE TABLE transactions (
	id SERIAL,
	user_id int not null,
  amount int not null,
  previous_amount int not null,
  new_amount int not null
);
```
Insert row
```psql
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, 0, 100);
```
New row in transaction
```psql
START TRANSACTION;
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1), (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1) + 100);
COMMIT;
```
Parallel transactions insert new amount:
T1: Insert new row
```psql
START TRANSACTION;
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1), (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1) + 100);
```
T2: insert new row
```psql
START TRANSACTION;
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1), (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1) + 100);
```
T1 and T2: Commit
```
COMMIT;
```
Result:
```console
test=# SELECT * FROM transactions;
 id | user_id | amount | previous_amount | new_amount 
----+---------+--------+-----------------+------------
  1 |       1 |    100 |               0 |        100
  2 |       1 |    100 |             100 |        200
  3 |       1 |    100 |             100 |        200
  4 |       1 |    100 |             100 |        200
(4 rows)
```
> So the last two rows have the same data, but I guess we want not that!

Delete this wrong rows (for clear example)
```psql
DELETE FROM transactions WHERE id IN (3, 4);
```

We can block last row (SELECT FOR UPDATE) until transaction end so no other transaction cannot take same row:
```psql
START TRANSACTION;
SELECT amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1 FOR UPDATE;
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1), (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1) + 100);
COMMIT;
```

T1: Start transaction, block last row and insert new one
```psql
START TRANSACTION;
SELECT amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1 FOR UPDATE;
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1), (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1) + 100);
```
> Not commit yet

T2: 
```psql
START TRANSACTION;
SELECT amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1 FOR UPDATE;
```
> T2 block on select statement until first transaction will commit or rollback

T1: commit transactions
```psql
COMMIT;
```
T2: insert and commit
```
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1), (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1) + 100);
COMMIT;
```
Result:
```console
test=# SELECT * FROM transactions;
 id | user_id | amount | previous_amount | new_amount 
----+---------+--------+-----------------+------------
  1 |       1 |    100 |               0 |        100
  2 |       1 |    100 |             100 |        200
 10 |       1 |    100 |             200 |        300
 11 |       1 |    100 |             300 |        400
(4 rows)
```
> Looks very good :)

But be carefully! Same doesn't work with isolation level REPEATABLE READ!

Example:
T1:
```psql
START TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SELECT amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1 FOR UPDATE;
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1), (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1) + 100);
```
T2:
```psql
START TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SELECT amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1 FOR UPDATE;
```
> Will block until first transaction commit or rollback;

T1: Commit
```psql
COMMIT;
```
T2: Insert and commit
```psql
INSERT INTO transactions(user_id, amount, previous_amount, new_amount) VALUES(1, 100, (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1), (SELECT new_amount FROM transactions WHERE user_id = 1 ORDER BY id DESC LIMIT 1) + 100);
COMMIT;
```
Result:
```console
test=# SELECT * FROM transactions;
 id | user_id | amount | previous_amount | new_amount 
----+---------+--------+-----------------+------------
  1 |       1 |    100 |               0 |        100
  2 |       1 |    100 |             100 |        200
 10 |       1 |    100 |             200 |        300
 11 |       1 |    100 |             300 |        400
 12 |       1 |    100 |             400 |        500
 13 |       1 |    100 |             400 |        500
(6 rows)
```
> Last two rows the same because in isolation level repeatable read transactions don't see changes of each other!


# MYSQL
Open mysql session
```
docker exec -it mysql_db mysql -uroot -proot
```
Use test database;
```
USE test;
```
Create transactions table
```
create table transactions(id int not null auto_increment, user_id int not null, amount int not null, primary key (id));
```
Insert few values
```
INSERT INTO transactions(user_id, amount) VALUES (1, 100);
INSERT INTO transactions(user_id, amount) VALUES (2, 200);
```
Check transaction isolation
```
mysql> SELECT @@transaction_ISOLATION;
+-------------------------+
| @@transaction_ISOLATION |
+-------------------------+
| REPEATABLE-READ         |
+-------------------------+
1 row in set (0.00 sec)
```
Again, 2 terminals, 2 sessions
#### MYSQL read uncommited
In both terminals:
```
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
```
Check
```console
mysql> SELECT @@transaction_ISOLATION;
+-------------------------+
| @@transaction_ISOLATION |
+-------------------------+
| READ-UNCOMMITTED        |
+-------------------------+
1 row in set (0.00 sec)
```
T1: Start transaction and insert row
```console
mysql> START TRANSACTION;
Query OK, 0 rows affected (0.00 sec)

mysql> INSERT INTO transactions(user_id, amount) VALUES(3, 300);
Query OK, 1 row affected (0.00 sec)

mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
|  3 |       3 |    300 |
+----+---------+--------+
3 rows in set (0.00 sec)
```
T2: Start transaction and select rows
```console
mysql> START TRANSACTION;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
|  3 |       3 |    300 |
+----+---------+--------+
3 rows in set (0.00 sec)
```
> We see 3 rows (third was inserted buy T1 transaction but was not commited, anyway we see this row in separate transcation)

T1: If we rollback first transaction, third row dissapear from second transaction result
```
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
|  3 |       3 |    300 |
+----+---------+--------+
3 rows in set (0.00 sec)

mysql> ROLLBACK;
Query OK, 0 rows affected (0.00 sec)
```
T2: 
```
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```

#### REPRODUCE PHANTOM READ (in REPEATABLE-READ Isolation);
Set REPEATABLE READ isolation level
```
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
```
T1: Check table
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
T1: Start transaction
```console
mysql> START TRANSACTION;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
T2: Insert new row in transaction
```
START TRANSACTION;
INSERT INTO transactions(user_id, amount) VALUES(3, 300);
COMMIT;
```
T1: Again check all table
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
T1: But we can update row with user_id = 3;
```console
mysql> UPDATE transactions SET amount = amount + 100 WHERE user_id = 3;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0
```
And after it we can see in T1 transaction different output with select * from transactions
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
|  3 |       3 |    400 |
+----+---------+--------+
3 rows in set (0.00 sec)
```
#### IN POSTGRES IT'S NOT HAPPEN
T1: Start transaction and select all rows
```console
test=# START TRANSACTION ISOLATION LEVEL REPEATABLE READ;
START TRANSACTION
test=# SELECT * FROM transactions;
 id | user_id | amount 
----+---------+--------
  1 |       1 |    100
  2 |       2 |    200
  3 |       3 |    300
(3 rows)
```
T2: Start transaction and insert new row
```console
START TRANSACTION ISOLATION LEVEL REPEATABLE READ;
INSERT INTO transactions(user_id, amount) VALUES(4, 400);
COMMIT;
```
T1: Try update new inserted row
```console
test=# UPDATE transactions SET amount = amount + 100 WHERE user_id = 4;
UPDATE 0
```
T1: Select all from transactions
```console
test=# SELECT * FROM transactions;
 id | user_id | amount 
----+---------+--------
  1 |       1 |    100
  2 |       2 |    200
  3 |       3 |    300
(3 rows)
```

# I know it's a little bit a mess but I want to repeat

## MYSQL READ UNCOMMITED
> A transaction can see changes to rows made by other transactions even before the transactions have been committed.

##### Nonrepeatable reads example (and Dirty reads also)
> When a transaction runs an identical SELECT statement twice and gets different results.

T1:
```mysql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
START TRANSACTION;
SELECT * FROM transactions;
```
Result
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
T2: 
```mysql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
START TRANSACTION;
UPDATE transactions SET amount = 300 WHERE user_id = 2;
```
T1:
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    300 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
**Row with user_id 2 now have amount eq 300**


##### Phantom rows example
> When one transaction runs a SELECT, and then another transaction performs an INSERT, and then the first transaction runs the same SELECT again and sees the new row.

T1:
```mysql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
START TRANSACTION;
SELECT * FROM transactions;
```
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
T2:
```mysql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
START TRANSACTION;
INSERT INTO transactions(user_id, amount) VALUES(3, 300);
```
> Not yet committed

T1:
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
|  4 |       3 |    300 |
+----+---------+--------+
3 rows in set (0.00 sec)
```
**We see new row in first transaction despite of second transaction not yet committed it.**

## MYSQL READ COMMITTED
> A transaction can only see changes to rows made by other transactions when they have been committed.

##### Dirty reads not possible

T1:
```mysql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
START TRANSACTION;
```
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
T2:
```mysql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
START TRANSACTION;
UPDATE transactions SET amount = 300 WHERE user_id = 2;
```
T1:
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
> T1 don't see changes of T2

##### Nonrepeatable Reads

T1:
```mysql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
START TRANSACTION;
```
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 |    200 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
T2:
```mysql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
START TRANSACTION;
UPDATE transactions SET amount = 100500 WHERE user_id = 2;
COMMIT;
```
> Update and commit changes

T1:
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 | 100500 |
+----+---------+--------+
2 rows in set (0.00 sec)
```
> T1 See committed changes of second transaction and get different result

##### Phantom rows

T1:
```mysql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
START TRANSACTION;
```
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 | 100500 |
+----+---------+--------+
2 rows in set (0.00 sec)

```
T2:
```
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
START TRANSACTION;
INSERT INTO transactions(user_id, amount) VALUES(5, 500);
COMMIT;
```
T1:
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 | 100500 |
|  5 |       5 |    500 |
+----+---------+--------+
3 rows in set (0.00 sec)
```
> T1 See new inserted row

## MYSQL REPEATABLE READ
> If a transaction performs a given SELECT statement twice, the result will always be the same, even if another transaction has changed rows in the meantime.

##### Dirty reads, Nonrepeatable reads, Phantom rows now possible by docs
> Check all together
T1:
```mysql
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
START TRANSACTION;
```
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 | 100500 |
|  5 |       5 |    500 |
+----+---------+--------+
3 rows in set (0.00 sec)
```
T2: Update exist row and insert new row
```mysql
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
START TRANSACTION;
UPDATE transactions SET amount = 199 WHERE user_id = 1;
INSERT INTO transactions(user_id, amount) VALUES(6, 600);
COMMIT;
```
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    199 |
|  2 |       2 | 100500 |
|  5 |       5 |    500 |
|  6 |       6 |    600 |
+----+---------+--------+
4 rows in set (0.00 sec)
```
T1: 
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 | 100500 |
|  5 |       5 |    500 |
+----+---------+--------+
3 rows in set (0.00 sec)
```
> T1 don't see changes even if they commit
**But we can make some hack for start to see new row**

T1:
```console
mysql> UPDATE transactions SET amount = 699 WHERE user_id = 6;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0
```
```console
mysql> SELECT * FROM transactions;
+----+---------+--------+
| id | user_id | amount |
+----+---------+--------+
|  1 |       1 |    100 |
|  2 |       2 | 100500 |
|  5 |       5 |    500 |
|  6 |       6 |    699 |
+----+---------+--------+
4 rows in set (0.00 sec)
```
> **Now in REPEATABLE READ isolation level we see new row inserted by another transaction!